let AutoLib = (()=>{
    let automobili = {
        1: {proizvodjac: "Proizvodjac1", model: "Model1", cena: 20000},
        2: {proizvodjac: "Proizvodjac2", model: "Model2", cena: 30000},
        3: {proizvodjac: "Proizvodjac3", model: "Model3", cena: 40000},
    }
    return {
        dobaviAuto(id, callback) {
            setTimeout(() => callback({...automobili[id]}), Math.random()*2000);
        }
    };
})();